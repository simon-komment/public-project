defmodule SimpleTracking.Shares do
  require Logger

  import Ecto.Query, warn: false
  alias SimpleTracking.Repo

  alias SimpleTracking.Shares.Share

  def list_shares do
    Repo.all(Share)
  end

  def get_share(id), do: Repo.get(Share, id)

  def get_share!(id), do: Repo.get!(Share, id)

  @spec find_shares_by_tracker(any) :: any
  def find_shares_by_tracker(tracker_id) do
    query = from s in "shares",
          where: s.tracker_id == ^tracker_id
    Repo.all(query)
  end

  @spec create_share(:invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}) ::
          any
  def create_share(attrs \\ %{}) do
    %Share{}
    |> Share.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a share.

  ## Examples

      iex> update_share(share, %{field: new_value})
      {:ok, %Share{}}

      iex> update_share(share, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_share(%Share{} = share, attrs) do
    share
    |> Share.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a share.

  ## Examples

      iex> delete_share(share)
      {:ok, %Share{}}

      iex> delete_share(share)
      {:error, %Ecto.Changeset{}}

  """
  def delete_share(%Share{} = share) do
    Repo.delete(share)
  end

  @doc """
  Returns false if the Share has no expiresAt
  Returns true if the Share has an expiresAt and is set to expire at a date in the future
  Returns true if the Share is set to expire at a date in the past
  """
  def expired(%Share{expires_at: nil}) do
    false
  end

  def expired(%Share{expires_at: expires_at}) do
    # Share was a typical time-based share
    {:ok, expires_at} = NaiveDateTime.from_iso8601("#{expires_at}")
    difference = NaiveDateTime.diff(expires_at, NaiveDateTime.local_now())

    Logger.info "#{difference}"

    # Should it have expired already?
    difference < 0
  end

  def get_status(%Share{} = share) do
    if expired(share) do
      :expired
    else
      if share.start == nil do
        :inactive_place
      else
        {:ok, start} = NaiveDateTime.from_iso8601("#{share.start}")
        difference = NaiveDateTime.diff(start, NaiveDateTime.local_now())
        if difference > 0 do
          :inactive_early
        else
          :active
        end
      end
    end
  end

  def start_share(%Share{} = share) do
    update_share(share, %{"start" => NaiveDateTime.local_now()})
  end
end
